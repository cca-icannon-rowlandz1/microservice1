const express = require('express')
const cors = require('cors')
const app = express();
app.use(cors());
var port = process.env.PORT || 8080;
app.use(express.urlencoded({extended: false}));
app.listen(port, () => console.log(`HTTP Server with Express.js listening on port ${port}`));

app.get('/', (req, res) => {
  res.send("Microservice1 gateway by Zachary Rowland and Ian Cannon. Usage: host/calc?number=_&numba=_");
});

app.get('/calc', function (req, res) {
    number=Number(req.query['number'])
    numba =Number(req.query['numba'])
    sum=number+numba
    dif=number-numba

    if(isNaN(number)||isNaN(numba)){
        res.send("Invalid inputs. Try again")
    }
    res.send(`${number}+${numba}=${sum}     
        ${number}-${numba}=${dif}`);
});
